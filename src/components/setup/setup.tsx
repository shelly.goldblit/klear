import { useReducer } from "react";
import { reducer } from "../../context/reducer";
import { SetupDispatchContext, SetupStateContext } from "../../context/setup-context";
import { NEXT_STEP, PREV_STEP, SET_NAME, StepData } from "../../types";
import { BrandsForm } from "../brands-form/brands-form";
import { ExpertiesForm } from "../experties-form/experties-form";
import { NameForm } from "../name-form/name-form";
import { Step } from "../step/step";

export interface SetupProps {
}

export const Setup = (props: SetupProps) => {

  const [state, dispatch] = useReducer(reducer, { userData: {name: ''}, currentStep: 0});
  const { currentStep} = state;
  const onNext= (() => dispatch({type:NEXT_STEP}));
  const onPrev= (() => dispatch({type:PREV_STEP}));

  const onNameChange = (e: React.FormEvent<HTMLInputElement>) => {
    dispatch({type: SET_NAME, name: e.currentTarget.value });
  }

  const steps:  StepData[] = [
    { title: 'Name', subTitle:'whatever', form: <NameForm onChange={onNameChange} name={state.userData.name}/>},
    { title: 'Experties '+ state.userData.name, subTitle:'whatever', form: <ExpertiesForm/>},
    { title: 'Brands', subTitle:'whatever', form: <BrandsForm/>},
  ]

  return (
    <SetupDispatchContext.Provider value={dispatch}>
      <SetupStateContext.Provider value={state}>
        <div>
          <Step 
            title={steps[currentStep].title || 'hello'}
            subTitle={steps[currentStep].subTitle || 'welcome'}
            form = {steps[currentStep].form}
          />
          <div>
          {currentStep > 0 && <button onClick={onPrev}>Prev</button>}
          {currentStep < steps.length-1 && <button onClick={onNext}>Next</button>}
          </div>
        </div>
      </SetupStateContext.Provider>
    </SetupDispatchContext.Provider>
);
}