export interface StepProps {
  title: string;
  subTitle: string;
  form: React.ReactElement;
}

export const Step = (props: StepProps) =>
{
  const {title, subTitle, form} = props;
  return (
    <>
      <h1>{title}</h1>
      <h2>{subTitle}</h2>
      {form}
    </>
  );
}