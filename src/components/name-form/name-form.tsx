
export interface NameFormProps {
  name: string;
  onChange: (e: React.FormEvent<HTMLInputElement>) => void;
}
export const NameForm = (props: NameFormProps) => {

  const {name, onChange} = props;
  return (<div>
    Name
    <input 
      type="text" 
      placeholder="name" 
      value={name} 
      onChange={onChange}
    />
</div>);
}