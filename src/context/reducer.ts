
import { Action, SET_NAME, SET_BRANDS, SET_EXPERTIES, State, NEXT_STEP, PREV_STEP } from '../types';



export const  reducer = (state: State, action: Action): State =>  {
  const { userData, currentStep } = state;
  switch (action.type) {
    case SET_NAME:
      return { ...state, userData: { ...userData, name: action.name} };
    case SET_BRANDS:
      return { ...state, userData: { ...userData, brandNames: action.brands} };
    case SET_EXPERTIES:
      return { ...state, userData: { ...userData, experties: action.experties} };
    case NEXT_STEP:
      return { ...state, currentStep: currentStep+1 };
    case PREV_STEP:
      return { ...state, currentStep: currentStep-1 };
    default:
      throw new Error();
  }
}
