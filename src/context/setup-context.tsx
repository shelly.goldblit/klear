import { createContext, useContext } from 'react';
import { Action, State } from '../types';

export const SetupStateContext = createContext({} as State);
export const SetupDispatchContext = createContext(({}) as React.Dispatch<Action>);

export const useState= () => useContext(SetupStateContext);
export const useCurrentStep= () => useState().currentStep;
export const useName= ()=> useState().userData.name || '';
export const useDispatch= () => useContext(SetupDispatchContext);
