export interface UserData {
  name: string;
  experties?: string[];
  brandNames?: string[];
}

export interface StepData {
  title: string;
  subTitle: string;
  form: React.ReactElement
}


export type State = {
  currentStep: number;
  userData: UserData;
};

export const SET_NAME = 'SET_NAME';
export const SET_EXPERTIES = 'SET_EXPERTIES';
export const SET_BRANDS = 'SET_BRANDS';
export const NEXT_STEP = 'NEXT_STEP';
export const PREV_STEP = 'PREV_STEP';

export type Action =
  | { type: 'SET_NAME' ; name: string; }
  | { type: 'SET_EXPERTIES'; experties: string[] }
  | { type: 'SET_BRANDS'; brands: string[] }
  | { type: 'NEXT_STEP' }
  | { type: 'PREV_STEP'}
